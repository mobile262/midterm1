import 'package:flutter/material.dart';
import 'package:regdesign/ScreenPage/calendar.dart';
import 'package:regdesign/ScreenPage/contact.dart';
import 'package:regdesign/ScreenPage/formdownload.dart';
import 'package:regdesign/ScreenPage/info.dart';
import 'package:regdesign/ScreenPage/timetable.dart';

void main() {
  runApp(Reg());
}

class MyApptheme {}

class Reg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Burapha University",
              style: TextStyle(color: Colors.black)),
          backgroundColor: Color.fromARGB(255, 233, 212, 152),
          actions: <Widget>[],
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                accountName: Text("62160242 : จิราธิป สุวรรณศิลป์ "),
                accountEmail: Text("62160250@go.buu.ac.th"),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://reg.buu.ac.th/registrar/getstudentimage.asp?id=62160250'),
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXc9YyhrZhhiv-_Qm8hkfV6oZ_9FFNbEXYaLaoKG3mW2eKiV93TDMWiloS6yhEjmTmmHU&usqp=CAU",
                    ),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("หน้าแรก"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Home()),
                  );
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("ประวัตินิสิต"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => info()),
                  );
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("ตารางสอน"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => timetable()),
                  );
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("ปฏิทินการศึกษา"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return calender();
                  }));
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("ดาวน์โหลดแบบฟอร์ม"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return formdownload();
                  }));
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.directions),
                title: Text("ติดต่อเรา"),
                iconColor: Colors.black,
                onTap: (() async {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return contact();
                  }));
                }),
              ),
              Divider(
                color: Colors.grey,
              ),
            ],
          ),
        ),
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Theme(
                    data: ThemeData(
                      iconTheme: IconThemeData(
                        color: Color.fromARGB(255, 56, 53, 53),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 300,
                  child: Image.network(
                    'https://reg.buu.ac.th/document/grad652.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  width: double.infinity,
                  height: 150,
                  child: Image.network(
                    'https://reg.buu.ac.th/anno/Line.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.facebook,
                        ),
                        onPressed: () async {},
                      ),
                      Text("มหาวิทยาลัยบูรพา Burapha University")
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
