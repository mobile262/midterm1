import 'package:flutter/material.dart';

class timetable extends StatelessWidget {
  const timetable({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 213, 206, 206),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'ตารางสอน',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color.fromARGB(255, 233, 212, 152),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว',
              style: TextStyle(
                fontSize: 20,
                color: Colors.deepOrange,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ชื่อ: จิราธิป สุวรรณศิลป์',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'สถานภาพ: กำลังศึกษา',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'คณะ: คณะวิทยาการสารสนเทศ',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'อ.ที่ปรึกษา: อาจารย์วรวิทย์ วีระพันธุ์',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 140,
            child: Image.network(
              'https://media.discordapp.net/attachments/945737214007050360/1070112824090705990/image.png',
              fit: BoxFit.contain,
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://media.discordapp.net/attachments/945737214007050360/1070112889832214588/image.png',
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }
}
