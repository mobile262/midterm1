import 'package:flutter/material.dart';

class info extends StatelessWidget {
  const info({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 213, 206, 206),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'ประวัตินิสิต',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color.fromARGB(255, 233, 212, 152),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://reg.buu.ac.th/registrar/getstudentimage.asp?id=62160250',
              fit: BoxFit.contain,
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ระเบียบประวิติ',
              style: TextStyle(
                fontSize: 20,
                color: Colors.deepOrange,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'รหัสประจำตัว: 62160250',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'เลขบัตรประชาชน: 1102003191329',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ชื่อ: นายจิราธิป สุวรรณศิลป์',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ชื่อภาษาอังกฤษ: MR.JIRATIP SUWANNASIN',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'คณะ: วิทยาการสารสนเทศ',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'วิทยาเขต: บางแสน',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ระดับการศึกษา: ปริญญาตรี',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'จบการศึกษาจาก: บางบ่อวิทยาคม',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'อ.ที่ปรึกษา: อายารย์วรวิทย์ วีระพันธุ์',
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    );
  }
}
