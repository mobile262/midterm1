import 'package:flutter/material.dart';

class contact extends StatelessWidget {
  const contact({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 213, 206, 206),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'ติดต่อ',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color.fromARGB(255, 233, 212, 152),
      ),
      body: ListView(
        children: <Widget>[
          Divider(
            color: Colors.grey,
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ติดต่อเรา',
              style: TextStyle(
                fontSize: 25,
                color: Colors.deepOrange,
              ),
            ),
          ),
          Table(
            border: TableBorder.all(),
            columnWidths: {
              0: FractionColumnWidth(0.01),
              1: FractionColumnWidth(0.5),
            },
            children: [
              buildRow([
                'เวลาทำการ',
                'เปิดภาคเรียน',
              ]),
              buildRow(
                  ['', ' 08:30 - 16:30 น. (อังคาร พฤหัสบดี เสาร์ อาทิตย์)']),
              buildRow(['', ' หมายเหตุ: ปิดวันหยุดราชการ']),
              buildRow(['', ' ปิดภาคเรียน']),
              buildRow(['', ' 08:30 - 16:30 น. (จันทร์ - ศุกร์)']),
              buildRow(['', ' ปิดเสาร์ - อาทิตย์']),
              buildRow([
                'ที่อยู่',
                ' กองทะเบียนและประมวลผลการศึกษา มหาวิทยาลัยบูรพา 169    ถ.ลงหาดบางแสน ต.แสนสุข   อ.เมือง จ.ชลบุรี 20131'
              ]),
              buildRow(['โทรศัพท์ (เคาน์เตอร์)', ' 0 3810 2725']),
              buildRow(['โทรสาร', '	0 3839 0441, 0 3810 2721']),
              buildRow([
                'โทรศัพท์ (เจ้าหน้าที่)',
                'ฝ่ายรับเข้าศึกษาระดับปริญญาตรี  โทร. 038-102721 หรือ 038-102643'
              ]),
              buildRow([
                '',
                'ฝ่ายทะเบียนการศึกษา                 ระดับบัณฑิตศึกษา                         โทร.038-102724 หรือ 038-102726'
              ]),
              buildRow(['', 'ระดับปริญญาตรี ']),
              buildRow(['', 'วิทยาเขตจันทบุรี (โทร. 038-102720)']),
              buildRow(['', 'วิทยาเขตสระแก้ว (โทร. 038-102729)']),
              buildRow(['', 'คณะศึกษาศาสตร์ (โทร. 038-102720)']),
              buildRow(['', 'คณะรัฐศาสตร์และนิติศาสตร์ (โทร. 038-102727)']),
              buildRow(['', 'คณะรัฐศาสตร์และนิติศาสตร์ (โทร. 038-102727)']),
              buildRow(['', 'คณะการจัดการและการท่องเที่ยว (โทร. 038-102729)']),
              buildRow(['', 'คณะมนุษยศาสตร์และสังคมศาสตร์ (โทร. 038-102723)']),
              buildRow(['', 'คณะวิศวกรรมศาสตร์ (โทร. 038-102723)']),
              buildRow(['', 'คณะศิลปกรรมศาสตร์ (โทร. 038-102723)']),
              buildRow(['', 'คณะดนตรีและการแสดง (โทร. 038-102723)']),
              buildRow(['', 'คณะพยาบาลศาสตร์ (โทร. 038-102726)']),
              buildRow([
                '',
                'การเทียบโอนผลการเรียน ตรวจสอบคุณวุฒิ  โทร. 038-102718'
              ]),
              buildRow(
                  ['', 'ตารางเรียน ตารางสอบ               โทร. 038-102715']),
              buildRow(['', 'จำนวนนิสิตและบุคคลภายนอก     โทร. 038102719']),
              buildRow(
                  ['', 'ระบบบริการการศึกษา               โทร. 038-102722']),
            ],
          ),
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  TableRow buildRow(List<String> cells) => TableRow(
        children: cells.map((cell) {
          return Padding(
            padding: const EdgeInsets.all(12),
            child: Center(child: Text(cell)),
          );
        }).toList(),
      );
}
