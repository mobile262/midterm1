import 'package:flutter/material.dart';

class formdownload extends StatelessWidget {
  const formdownload({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 213, 206, 206),
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'ดาวน์โหลดแบบฟอร์ม',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color.fromARGB(255, 233, 212, 152),
      ),
      body: ListView(
        children: <Widget>[
          Divider(
            color: Colors.grey,
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'ปฏิทินการศึกษา',
              style: TextStyle(
                fontSize: 25,
                color: Colors.deepOrange,
              ),
            ),
          ),
          Table(
            border: TableBorder.all(),
            columnWidths: {
              0: FractionColumnWidth(0.1),
              1: FractionColumnWidth(0.5),
            },
            children: [
              buildRow([' ที่', ' แบบคำร้อง (Request form)']),
              buildRow([' 1', 'RE01 คำร้องทั่วไป General Request form']),
              buildRow([' 2', 'RE02 คำร้องขอเงินค่าหน่วยกิตคืน']),
              buildRow([
                ' 3',
                'RE03 คำร้องขอใบแสดงผลการเรียน/ใบรับรอง/บัตรนิสิต Request form for transcript/certification letter/Student ID Card'
              ]),
              buildRow([
                ' 4',
                'RE04 คำร้องขอเปลี่ยนชื่อ นามสกุล คำนำหน้า หรือที่อยู่'
              ]),
              buildRow([' 5', 'RE05 คำร้องขอลงทะเบียนล่าช้า โดยเสียค่าปรับ']),
              buildRow([' 6', 'RE05-1 คำร้องขอเพิ่ม-ลดล่าช้า โดยเสียค่าปรับ']),
              buildRow([' 7', 'คำร้องขอเปลี่ยนวิชาเอก หรือย้ายคณะ']),
              buildRow([
                ' 8',
                'คำร้องขอคืนสภาพการเป็นนิสิตระดับปริญญาตรี คำร้องขอคืนสภาพการเป็นนิสิตระดับบัณฑิตศึกษา'
              ]),
              buildRow([
                ' 9',
                'RE09 คำร้องขอลงทะเบียนเกิน หรือต่ำกว่าหน่วยกิต ระดับปริญญาตรี'
              ]),
              buildRow([
                ' 10',
                'RE09-1 คำร้องขอลงทะเบียนเกิน หรือต่ำกว่าหน่วยกิต ระดับบัณฑิตศึกษา'
              ]),
              buildRow([
                ' 11',
                '	RE10 คำร้องลาพักการเรียน/รักษาสภาพ (สำหรับป.ตรีทุกระดับและบัณฑิตศึกษารหัส 60 ลงไป)   คำร้องรักษาสภาพบัณฑิตศึกษา รหัส 61 เป็นต้นไป'
              ]),
              buildRow([
                ' 12',
                '	RE11 คำร้องขอลาออกจาการเป็นนิสิต (ต้องยื่นพร้อมบัตรนิสิต)'
              ]),
              buildRow([
                ' 13',
                'RE11-1 คำร้องขอลาออกจากการเป็นนิสิต (นิสิตใหม่สละสิทธิ์)'
              ]),
              buildRow([' 14', 'RE12 บัตรเพิ่ม-ลดรายวิชาเรียน   ยกเลิกการใช้']),
              buildRow([
                ' 15',
                'RE13 คำร้องขอให้รายงานผลการศึกษา (สำหรับบัณฑิตศึกษา)'
              ]),
              buildRow(
                  [' 16', 'RE14 คำร้องเทียบโอนผลการเรียน (สำหรับปริญญาตรี)']),
              buildRow([' 17', 'RE16 คำร้องของดเรียน (Drop)']),
              buildRow([' 18', 'RE17 คำร้องขอลงทะเบียนเรียนซ้ำ หรือเรียนแทน']),
              buildRow([' 19', 'RE18 คำร้องขอลงทะเบียนข้ามประเภท']),
              buildRow([' 20', 'RE19 คำร้องขอเลื่อนการสอบ']),
              buildRow([' 21', 'RE20 คำร้องขอลงทะเบียนเรียนข้ามสถาบัน']),
              buildRow([' 22', 'RE21 คำร้องของดเรียนทุกรายวิชา']),
              buildRow([' 23', 'RE22 คำร้องขอลงทะเบียนเรียนแบบบุคคลภายนอก']),
              buildRow([' 24', 'แบบฟอร์มยินยอมให้เปิดเผยข้อมูล']),
              buildRow([' 25', 'RE24 คำร้องขอขยายเวลาชำระเงิน']),
            ],
          ),
        ],
      ),
    );
  }

  TableRow buildRow(List<String> cells) => TableRow(
        children: cells.map((cell) {
          return Padding(
            padding: const EdgeInsets.all(12),
            child: Center(child: Text(cell)),
          );
        }).toList(),
      );
}
